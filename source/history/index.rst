.. _history:

######################
Historique de versions
######################

v1.16.1 (13/04/2017)
====================

* Ajout d'une case à cocher sur le formulaire de génération du fichier CSV à destination de la préfecture, permettant d'ajouter dans le fichier le nombre de votant d’après les feuilles d’émargements.


v1.16 (23/03/2017)
==================

* Ajout de l'option de publication des résultats définitifs.
* Modification des résultats globaux dans l'animation en ajoutant le nombre d'inscrit des bureaux arrivés seulement. De plus un pourcentage de participation est calculé sur cette nouvelle donnée.
* Ajout de la possibilité de modifier le thème de l'animation directement depuis l'URL.
* Ajout de la possibilité de renseigner le nombre d'inscrit depuis la saisie des résultats à condition que l'option "votant2_procuration_saisie_form" soit activée.
* Ajout du nombre de votants sur liste d'émargement dans le fichiers CSV des résultats transmis en préfecture.
* Ajout du nouvel affichage "Résultats (liste des bureaux à gauche)" identique à l'affichage "Résultats" mais avec la liste des bureaux à gauche.
* Ajout du nouvel affichage "Résultats sans cumul (liste des bureaux à gauche)" permettant d'afficher la liste des bureaux à gauche et les résultats du bureau sélectionné à droite.
* Ajout du nouvel affichage "Résultats avec graphique du cumul (liste des bureaux à gauche)" permettant d'afficher un graphique pour les résultats cumulés et la liste des bureaux à gauche de l'écran.
* Ajout du titre "RÉSULTATS GLOBAUX" pour la colonne des résultats cumulés dans l'animation.
* Lors de la création d'une élection, la fréquence de participation est maintenant une donnée obligatoire, ce qui évite une erreur de base de données quand elle n'est pas renseignée.


v1.15 (19/03/2015)
==================

* Modification du formulaire des types d'élection pour ajouter plusieurs
  élections du même type sur la même période.
* Correction de l'affichage de certains états.
* Amélioration du contrôle de saisit dans la plupart des formulaires.
* Modification du nom des champs pour les candidats, ce n'est pas forcément nom
  et prénom à chaque fois. Pour chaque écran de l'application, le libellé 1
  (anciennement le nom) sera affiché avant le libellé 2 (anciennement le prénom)
  lorsqu'il s'agit d'un candidat.


v1.14 (21/05/2014)
==================

* Règlementaire : gestion séparée des votes blancs et des votes nuls.
* Remplacement des affichages WEB, MOBILE et BORNE par un affichage unique
  SITE WEB ADAPTATIF (web responsive design).
* Suppression de l'affichage WEB GOOGLE.
* Ajout d'un état PDF 'résultats globaux' supplémentaire.
* Ajout d'un état PDF 'proclamation' supplémentaire.


v1.13 (12/03/2014)
==================

* Ajout de la gestion de la répartition des sièges pour l'élection des 
  conseillers communautaires lors d'une élection municipale.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.
* Ajout de trois états PDF 'proclamation'.
* Ajout d'un état PDF 'résultats globaux'.


v1.12 (29/03/2012)
==================

* Nouvelle interface de saisie des résultats.
* Abandon du support PostGreSQL.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.


v1.11 (28/02/2010)
==================

* Ajout de l'affichage des résultats au public WEB GOOGLE identique à 
  l'affichage WEB existant avec une carte Google Map.
* Ajout de l'affichage des résultats au public MOBILE pour visualisation 
  adaptée aux smartphones.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.


v1.09 & v1.10 (23/06/2009)
==========================

* Ajout du support de la seconde centaine.


